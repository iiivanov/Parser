﻿-- 
-- Отключение внешних ключей
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Установить режим SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Описание для таблицы request
--
DROP TABLE IF EXISTS request;
CREATE TABLE request (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT ' Идентификатор строки поиска',
  str VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'Строка поиска',
  answerHash VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'Хеш ответа',
  PRIMARY KEY (id),
  UNIQUE INDEX IDX_request_str (str)
)
ENGINE = INNODB
AUTO_INCREMENT = 21
AVG_ROW_LENGTH = 3276
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы answers
--
DROP TABLE IF EXISTS answers;
CREATE TABLE answers (
  idStr INT(11) UNSIGNED NOT NULL COMMENT 'идентификатор строки поиска',
  url VARCHAR(255) NOT NULL COMMENT 'ссылка на видео',
  header VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'Заголовок',
  views INT(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'количество просмотров',
  descriprion VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'краткое описание',
  CONSTRAINT FK_answers_request_id FOREIGN KEY (idStr)
    REFERENCES request(id) ON DELETE CASCADE ON UPDATE NO ACTION
)
ENGINE = INNODB
AVG_ROW_LENGTH = 327
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'таблица ответов';

-- 
-- Вывод данных для таблицы request
--
INSERT INTO request VALUES
(1, 'adriano celentano', '438e9408d5b497963621db38f772d4cd'),
(16, 'sting', 'd6447c45655976c142acf603bd7739e2'),
(18, 'pavarotti caruso', 'ca8fdcca8e0de786ede269c8d8a89f0f'),
(19, 'celine dion', '86b8cb35a7b2b769d310df590312fe6e'),
(20, 'michael jackson', '35040d9cecdd99899a8e4a275e3687f2');

-- 
-- Вывод данных для таблицы answers
--
INSERT INTO answers VALUES
(1, 'https://www.youtube.com/watch?v=kKfybNLrFUA', 'Adriano Celentano / Адриано Челентано — Confessa (alternative version)', 3343191, 'Новая версия известного клипа «Confessa» итальянского актёра и певца Адриано Челентано. 2002 год, альбом Per sempre ...'),
(1, 'https://www.youtube.com/watch?v=hwaQdpwNzC8', 'Adriano Celentano Verona Live 2012', 139466, ''),
(1, 'https://www.youtube.com/watch?v=KadQgizzeUM', 'adriano celentano - susana', 4647394, 'Adriano Celentano sings: "Susanna" 1984.'),
(1, 'https://www.youtube.com/watch?v=rlt3tp4QiBk', 'Adriano Celentano -Ma Perke', 17268777, 'Adriano Celentano -Ma Perke.'),
(1, 'https://www.youtube.com/watch?v=bu3Nmu_X6y4', 'Adriano Celentano - Io non so parlar d''amore (1999) [FULL ALBUM] 320 kbps', 4195961, 'CD Rip from 1999 album of Adriano Celentano. Tracklist: [00:01] - Gelosia [04:31] - L''emozione non ha voce [08:40] ...'),
(1, 'https://www.youtube.com/watch?v=JMEXn6A7n8Q', 'Adriano Celentano & Gianni Morandi - Ti Penso e Cambia il Mondo - Sanremo 2012', 154347, ''),
(1, 'https://www.youtube.com/watch?v=IrmP_ymsSZQ', 'Adriano Celentano beim Wein auspressen', 592326, ''),
(1, 'https://www.youtube.com/watch?v=VlooND9Jb2c', 'Adriano Celentano - Pregherò (Stand by me) (LIVE 2012)', 449332, 'Special for https://vk.com/club63184505.'),
(1, 'https://www.youtube.com/watch?v=wmmKcQ5ZsUk', 'Adriano Celentano - IL tempo Se Ne Va', 17774835, 'Musik.'),
(1, 'https://www.youtube.com/watch?v=iVa4WeZ3wJE', 'Adriano Celentano - Il ragazzo della via Gluck -Live Berlino/Official Video/Parole in descrizione', 667533, 'ISCRIVITI AL CANALE CLICCANDO QUI: http://goo.gl/EfGd8z Il Ragazzo della Via Gluck - Live a Berlino 1994 Lyrics: (coro) là ...'),
(16, 'https://www.youtube.com/watch?v=HQkkmYIu95I', 'Mylène Farmer, Sting - Stolen Car', 6592232, '""Stolen car"" 1er extrait du nouvel album de Mylène Farmer à paraître le 6 novembre Duo avec Sting produit par The Avener ...'),
(16, 'https://www.youtube.com/watch?v=C3lWwBslWqg', 'Sting - Desert Rose', 71767578, 'Music video by Sting performing Desert Rose. (C) 1999 A&M Records.'),
(16, 'https://www.youtube.com/watch?v=ezcQu1JtcPM', 'Sting - Shape of my heart -2(Леон)', 3343403, 'Sting - Shape of my heart -2(Леон)'),
(16, 'https://www.youtube.com/watch?v=0SmQc7jhGOM', 'Sting illuminates on his retirement: April 2, 2016', 196011, 'Sting explains why he decided to officially retire from the ring. More ACTION on WWE NETWORK : http://bit.ly/MobQRl Subscribe ...'),
(16, 'https://www.youtube.com/watch?v=lB6a-iD6ZOY', 'Sting - Fragile', 28242294, 'Music video by Sting performing Fragile. YouTube view counts pre-VEVO: 1794143. (C) 1991 A&M Records.'),
(16, 'https://www.youtube.com/watch?v=YyN53M4Wo5Q', 'Final Episode of WCW Nitro: Sting vs. Ric Flair', 90192, 'Sting defeats Ric Flair on the final episode of WCW Nitro on March 26th, 2001. More ACTION on WWE NETWORK ...'),
(16, 'https://www.youtube.com/watch?v=Ct7e-6RwMJY', 'The incomparable Sting gets inducted into immortally: 2016 WWE Hall of Fame on WWE Network', 549824, 'The Icon has climbed to the top of the mountain as the newest inductee to the 2016 WWE Hall of Fame. More ACTION on WWE ...'),
(16, 'https://www.youtube.com/watch?v=d27gTrPPAyk', 'Sting - Englishman In New York', 47701633, 'Music video by Sting performing Englishman In New York. (C) 1987 A&M Records.'),
(16, 'https://www.youtube.com/watch?v=037uSAIahho', 'Sting - Shape Of My Heart', 32957439, ''),
(16, 'https://www.youtube.com/watch?v=u1QAbdIEicw', 'Sting - Live in Viña del Mar (Full Concert HD) 2011', 2002935, 'Sting Live in Viña del mar & Orquesta Sinfónica de Chile 00:00 If I Ever Lose My Faith in You http://youtu.be/VATkqDkcb4s 04:42 ...'),
(18, 'https://www.youtube.com/watch?v=iDEdZfnULIg', 'Pavarotti - Caruso (english subtitles)', 717889, 'Luciano Pavarotti - Caruso. Live in Paris and with english lyrics/subtitles. please correct me if I have made any errors in translation ...'),
(18, 'https://www.youtube.com/watch?v=I8A61eY1Efg', 'Pavarotti "caruso"', 10924158, 'Pavarotti "caruso" live in Paris.'),
(18, 'https://www.youtube.com/watch?v=tRGuFM4DR2Y', 'Caruso (Live). Luciano Pavarotti & Lucio Dalla (HQ)', 6482544, 'Luciano Pavarotti interpreta "Caruso" junto a Lucio Dalla. Ocurrió durante el primero de los conciertos de Pavarotti & Friends de ...'),
(18, 'https://www.youtube.com/watch?v=KuDwsiXP00A', 'Luciano Pavarotti - Caruso ( Te voglio bene assai)', 1604807, 'English translation: Here, where the sea shines and the wind howls, on the old terrace beside the gulf of Sorrento, a man ...'),
(18, 'https://www.youtube.com/watch?v=9fs8ksaM80Q', 'Luciano Pavarotti - Caruso', 207648, ''),
(18, 'https://www.youtube.com/watch?v=9SBUeaNkifA', 'Luciano Pavarotti - Caruso', 777394, 'Lucio Dalla Qui dove il mare luccica e tira forte il vento su una vecchia terrazza davanti al golfo di Sorrento un uomo abbraccia ...'),
(18, 'https://www.youtube.com/watch?v=Es65K6AHKvo', 'Luciano Pavarotti - Caruso', 167756, 'Tema de Lucio Dalla Interpretado por el Gran Luciano Pavarotti. Letra en español: Aquí donde el mar reluce y sopla fuerte el ...'),
(18, 'https://www.youtube.com/watch?v=iJFI53qZ7xY', 'Luciano Pavarotti Caruso (HD)', 67537, ''),
(18, 'https://www.youtube.com/watch?v=Na6guEZws5c', 'Luciano Pavarotti Caruso', 70461, ''),
(18, 'https://www.youtube.com/watch?v=RlK9bigjnvk', 'Luciano Pavarotti  -  Caruso (Te voglio bene assai)', 692007, 'LYRICS & TRANSLATION-The original version was written by Lucio Dalla, who dedicated this song to Enrico Caruso, an italian ...'),
(19, 'https://www.youtube.com/watch?v=WNIPqafd4As', 'Céline Dion - My Heart Will Go On', 67679686, 'Music video by Céline Dion performing My Heart Will Go On. (C) 2007 Sony Music Entertainment Canada Inc.'),
(19, 'https://www.youtube.com/watch?v=RfhNS6r0FeQ', 'Best Of Celine Dion 2015 (HD/HQ)', 2268424, 'Best Of Celine Dion 2015 Playlist: 01. The Power Of Love 02. My Heart Will Go On 03. All By Mysefl 04. A New Day Has Come 05.'),
(19, 'https://www.youtube.com/watch?v=8M7x6FD2Ti0', 'Celine Dion - The Last Goodbye To René Angélil', 3217978, 'Rene Angelil''s Funeral: Pour Que Tu M''aimes Encore 22/01/2016.'),
(19, 'https://www.youtube.com/watch?v=xbO3dfF9uuE', 'Céline Dion - The Power of Love (Live in Boston)', 30800763, 'Music video by Céline Dion performing The Power Of Love. (C) 2008 Sony Music Entertainment Canada Inc.'),
(19, 'https://www.youtube.com/watch?v=NJsa6-y4sDs', 'Céline Dion - I''m Alive', 55217731, 'Music video by Céline Dion performing I''m Alive. (C) 2002 Sony Music Entertainment (Canada) Inc.'),
(19, 'https://www.youtube.com/watch?v=QvO5z5tx4ZA', 'Céline Dion - Incredible', 11520790, 'Céline Dion duet with Ne-Yo - Incredible Download “Incredible” on iTunes now at http://smarturl.it/lmbtl Music video by Céline ...'),
(19, 'https://www.youtube.com/watch?v=AzaTyxMduH4', 'Céline Dion - Pour que tu m''aimes encore', 24811543, 'Music video by Céline Dion performing Pour que tu m''aimes encore. (C) 1995 Sony Music Entertainment (Canada) Inc.'),
(19, 'https://www.youtube.com/watch?v=_LTsjwiTN7w', 'Céline Dion - Parler à mon père', 27231517, 'Music video by Céline Dion performing Parler à mon père. (C) 2012 Sony Music Entertainment Canada Inc. Procurez-vous en ...'),
(19, 'https://www.youtube.com/watch?v=T6wbugWrfLU', 'Céline Dion - That''s The Way It Is', 68408916, 'Music video by Céline Dion performing That''s The Way It Is. (C) 1999 Sony Music Entertainment (Canada) Inc.'),
(19, 'https://www.youtube.com/watch?v=6yR0sx9Y24o', 'Celine Dion - Hello (Adele Cover) LIVE - New Year''s Surprise - Dec 31st 2015', 6035742, 'New Year''s Eve Concert special number! The Colosseum @ Las Vegas (4K)'),
(20, 'https://www.youtube.com/watch?v=QNJL6nfu__Q', 'Michael Jackson - They Don''t Care About Us', 214548943, 'Music video by Michael Jackson performing They Don''t Care About Us. (C) 1996 MJJ Productions Inc.'),
(20, 'https://www.youtube.com/watch?v=LeiFF0gvqcc', 'Michael Jackson - Remember The Time', 145197375, 'Music video by Michael Jackson performing Remember The Time. (C) 1992 MJJ Productions Inc.'),
(20, 'https://www.youtube.com/watch?v=rAiw2SXPS-4', 'Michael Jackson "Billie Jean" 30th Anniversary Madison Square Garden NY', 21172627, 'Michael Jackson "Billie Jean" 30th Anniversary Madison Square Garden NY Subscribe Vlog Channel: ...'),
(20, 'https://www.youtube.com/watch?v=XAi3VTSdTxU', 'Michael Jackson - Earth Song', 90937968, 'Music video by Michael Jackson performing Earth Song. © 1995 MJJ Productions Inc.'),
(20, 'https://www.youtube.com/watch?v=4Aa9GwWaRv0', 'Michael Jackson - Smooth Criminal - Live in Munich 1997', 21786011, 'Michael Jackson - Smooth Criminal - HIStory World Tour Live in Munich 1997 ♥♥♥MICHAEL JACKSON.'),
(20, 'https://www.youtube.com/watch?v=sOnqjkJTMaA', 'Michael Jackson - Thriller', 312605502, 'Music video by Michael Jackson performing Thriller. (C) 1982 MJJ Productions Inc. #VEVOCertified on October 29, 2010.'),
(20, 'https://www.youtube.com/watch?v=Zi_XLOBDo_Y', 'Michael Jackson - Billie Jean', 204516334, 'Music video by Michael Jackson performing Billie Jean. © 1982 MJJ Productions Inc.'),
(20, 'https://www.youtube.com/watch?v=F2AitTPI5U0', 'Michael Jackson - Black Or White', 113441569, 'Music video by Michael Jackson performing Black Or White. © 1991 MJJ Productions Inc.'),
(20, 'https://www.youtube.com/watch?v=LJ7qXHjxj_0', 'Michael Jackson - Give In To Me', 30681855, 'Music video by Michael Jackson performing Give In To Me. (C) 1993 MJJ Productions Inc.'),
(20, 'https://www.youtube.com/watch?v=PIWSyUHD_nk', 'IF MICHAEL JACKSON MADE TRAP MUSIC [PART 2] "Billie Jean I Dab"', 98348, 'MICHAEL TRAPSON IS BACK AT IT AGAIN! LMAO Listen to Michael Trapson - Billie Jean & I Dab (Official Song) Prod by ...');

-- 
-- Восстановить предыдущий режим SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Включение внешних ключей
-- 
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;