<?php
/**
  * 
  * @author     Ivan Ivanov
  * @author     Ivan Ivanov <iiivanov.ukraine@gmail.com>
  */

require_once(dirname(__FILE__).'/parser/youtube.php');
require_once(dirname(__FILE__).'/db/fBlock.php');
require_once(dirname(__FILE__).'/db/setings.php');


if(fBlock::isAjax()) {
    if(isset($_POST['searchString'])) {
        $yt = new YoutubeParser();
        $result = $yt->searchVideo($_POST['searchString']);
        
        $db = new fBlock($setingsDB);
        $aHash = $db->makeHesh($result);
        $searchStringDate = $db->getSearchStringDate($_POST['searchString']);
        
        if($searchStringDate['error'] > 0) {
            die(json_encode($searchStringDate));
        } else {
            if($searchStringDate['answerHash'] <> $aHash) {
                $r = $db->addNewAnswer($searchStringDate['id'], $aHash, $result);
                if($r['error'] > 0) {
                    die(json_encode($r));
                }
            }
            $avgResult = $db->getAvg($searchStringDate['id']);
            if($avgResult['error'] > 0) {
                die(json_encode($avgResult));
            }
        }
        
        $data = array(
            'error' => '0',
            'searchString' => $searchStringDate['str'],
            'count' => $avgResult['count'],
            'views' => $avgResult['avg'],
            'result' => $result,
        );
    } else {
        $data = array(
            'error' => '1',
            'errorMessage' => 'No data',
        );
    }
    
    echo json_encode($data);
}