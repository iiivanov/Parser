function addSearchString() {
    var xhr = new XMLHttpRequest();
    
    var body = 'searchString=' + encodeURIComponent(document.getElementById('searchString').value);
    xhr.open('POST', 'add.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    xhr.send(body);
    xhr.onreadystatechange = function() {
        if (xhr.readyState != 4) return;

        if (xhr.status != 200) {
            alert(xhr.status + ': ' + xhr.statusText);
        } else {
            var data = JSON.parse(xhr.responseText);
            if(data['error'] != 0) {
                alert(data['errorMessage']);
            } else {
                var pTag = document.getElementById('list');
                var newlink = document.createElement('span');
                newlink.setAttribute('onclick', 'search(this)');
                newlink.innerHTML = data['searchString'];
                pTag.innerHTML = ', ' + pTag.innerHTML;
                pTag.insertBefore(newlink, pTag.firstChild);
            }
        }
    }
}

function search(event) {
    var xhr = new XMLHttpRequest();
    var body = 'searchString=' + encodeURIComponent(event.innerHTML);
    xhr.open('POST', 'search.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    xhr.send(body);
    xhr.onreadystatechange = function() {
        if (xhr.readyState != 4) return;

        if (xhr.status != 200) {
            alert(xhr.status + ': ' + xhr.statusText);
        } else {
            var data = JSON.parse(xhr.responseText);
            if(data['error'] != 0) {
                alert(data['errorMessage']);
            } else {
                document.getElementById('autor').innerHTML = data['searchString'];
                document.getElementById('count').innerHTML = data['count'] + ' записей в БД';
                document.getElementById('views').innerHTML = data['views'] + ' среднее количество просмотров';
                
                var r = data['result'];
                var pTag = document.getElementById('full-data');
                pTag.innerHTML = '';
                var tableItem;
                for (var i = 0; i < r.length; i++) {
                    tableItem = document.createElement('tr');
                    tableItem.innerHTML = 
                        '<td>'+(i+1)+'</td>' +
                        '<td><a href="'+r[i]['videoUrl']+'">'+r[i]['videoUrl']+'</a></td>' +
                        '<td>'+r[i]['header']+'</td>' +
                        '<td>'+r[i]['views']+'</td>' +
                        '<td>'+r[i]['desc']+'</td>';
                    pTag.appendChild(tableItem);
                }
            }
        }
    }
}