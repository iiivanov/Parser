<?php
error_reporting(E_ALL);
/**
  * 
  * @author     Ivan Ivanov
  * @author     Ivan Ivanov <iiivanov.ukraine@gmail.com>
  */

require_once(dirname(__FILE__).'/db/homePage.php');
require_once(dirname(__FILE__).'/db/setings.php');

$db = new homePage($setingsDB);
$data = $db->getSearchStrings();

if(is_file(dirname(__FILE__).'/view/homeView.php')) {
    require dirname(__FILE__).'/view/homeView.php';
}