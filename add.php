<?php
/**
  * 
  * @author     Ivan Ivanov
  * @author     Ivan Ivanov <iiivanov.ukraine@gmail.com>
  */

require_once(dirname(__FILE__).'/db/zBlock.php');
require_once(dirname(__FILE__).'/db/setings.php');

if(zBlock::isAjax()) {
    if(isset($_POST['searchString'])) {
        $db = new zBlock($setingsDB);
        $data = $db->addNewSearchString($_POST['searchString']);
    } else {
        $data = array(
            'error' => '1',
            'errorMessage' => 'No data',
        );
    }
    
    echo json_encode($data);
}