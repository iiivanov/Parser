<?php
/**
 * zBlock
 * @category   Database
 * @author     Ivan Ivanov
 * @author     Ivan Ivanov <iiivanov.ukraine@gmail.com>
 */

require_once(dirname(__FILE__).'/dataBase.php');

class zBlock extends dataBase {
    
    /**
     * 
     * @param type $searchString
     * @return type
     */
    public function addNewSearchString($searchString) {
        try {
            $sql = 'INSERT INTO request (str, answerHash) VALUES (:str, "");';
            $query = $this->_connection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $query->execute(array(
                ':str' => $searchString,
            ));
            
            $lastInsertId = $this->_connection->lastInsertId();
            
            $data = array(
                'error' => '0',
                'lastInsertId' => $lastInsertId,
                'searchString' => $searchString,
            );
            
        } catch(PDOException $e) {
            $data = array(
                'error' => '1',
                'errorMessage' => $e->getMessage(),
            );
        }
        
        return $data;
    }
}
