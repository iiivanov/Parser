<?php

/**
 * Database
 * @category   Database
 * @author     Ivan Ivanov
 * @author     Ivan Ivanov <iiivanov.ukraine@gmail.com>
 */
class dataBase {
    
    protected $_connection;

    protected $_config;
    
    /**
     * 
     * @param array $config
     * 
     * @return  void
     */
    public function __construct(array $config) {
        $this->_config = $config;
        $this->connect();
    }
    
    /**
     * 
     * @return void
     */
    public function connect() {
        if ($this->_connection)
                return;

        extract($this->_config);

        try {
            $this->_connection = new PDO('mysql:host='.$host.';dbname='.$dataBase, $userName, $password);
            $this->_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->_connection->exec("set names utf8");
        } catch(PDOException $e) {
            exit($e->getMessage());
        }
    }
    
    /**
     * 
     * @return void
     */
    public function __destruct() {
        $this->disconnect();
    }
    
    /**
     * 
     * @return boolean
     */
    public function disconnect() {
        $this->_connection = null;

        return true;
    }
    
    public static function isAjax() {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH'])
            && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }
}