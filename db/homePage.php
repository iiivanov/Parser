<?php
/**
 * homePage
 * @category   Database
 * @author     Ivan Ivanov
 * @author     Ivan Ivanov <iiivanov.ukraine@gmail.com>
 */

require_once(dirname(__FILE__).'/dataBase.php');

class homePage extends dataBase {
    
    /**
     * 
     * @return array
     */
    
    public function getSearchStrings() {
        try {
            $query = $this->_connection->query('SELECT r.str FROM request r ORDER BY r.id ASC');
            $query->setFetchMode(PDO::FETCH_ASSOC);
            $data = $query->fetchAll();
            
        } catch(PDOException $e) {
            $data = array('error' => $e->getMessage());
        }
        
        return $data;
    }
}
