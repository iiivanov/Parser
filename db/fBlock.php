<?php
/**
 * fBlock
 * @category   Database
 * @author     Ivan Ivanov
 * @author     Ivan Ivanov <iiivanov.ukraine@gmail.com>
 */


require_once(dirname(__FILE__).'/dataBase.php');

class fBlock extends dataBase {
    
    /**
     * 
     * @param type $searchString
     * @return type
     */
    public function getSearchStringDate($searchString) {
        try {
            $sql = 'SELECT r.id, r.str, r.answerHash FROM request r WHERE r.str = :str;';
            $query = $this->_connection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $query->execute(array(
                ':str' => $searchString,
            ));
            
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
            
            $data = array(
                'error' => '0',
                'id' => $result[0]['id'],
                'str' => $result[0]['str'],
                'answerHash' => $result[0]['answerHash'],
            );
            
        } catch(PDOException $e) {
            $data = array(
                'error' => '1',
                'errorMessage' => $e->getMessage(),
            );
        }
        
        return $data;
    }
    
    /**
     * 
     * @param array $data
     * @return string
     */
    public function makeHesh(array $data) {
        $ctx = hash_init('md5');
        foreach ($data as $value) {
            hash_update($ctx, serialize($value));
        }
        return hash_final($ctx);
    }
    
    public function addNewAnswer($id, $aHash, array $result) {
        try {
            $sql = 'UPDATE request r SET r.answerHash = :aHash WHERE r.id = :id;';
            $query = $this->_connection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $a = $query->execute(array(
                ':id' => $id,
                ':aHash' => $aHash,
            ));
            
            $sql = 'DELETE FROM answers WHERE idStr = :id;';
            $query = $this->_connection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $query->execute(array(
                ':id' => $id,
            ));
            
            foreach ($result as $value) {
                $sql = 'INSERT INTO answers (idStr, url, header, views, descriprion)'
                    .'VALUES (:id, :url, :header, :views, :descriprion);';
                $query = $this->_connection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $query->execute(array(
                    ':id' => $id,
                    ':url' => $value['videoUrl'],
                    ':header' => $value['header'],
                    ':views' => $value['views'],
                    ':descriprion' => $value['desc'],
                ));
            }
            
            $data = array(
                'error' => '0',
            );
            
        } catch(PDOException $e) {
            $data = array(
                'error' => '1',
                'errorMessage' => $e->getMessage(),
            );
        }
        
        return $data;
    }

    public function getAvg($id) {
        try {
            $sql = 'SELECT COUNT(a.views) AS `count`, ROUND(AVG(a.views)) AS `avg` '
                . 'FROM answers a WHERE a.idStr = :id;';
            $query = $this->_connection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $query->execute(array(
                ':id' => $id,
            ));
            
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
            
            $data = array(
                'error' => '0',
                'count' => $result[0]['count'],
                'avg' => $result[0]['avg'],
            );
            
        } catch(PDOException $e) {
            $data = array(
                'error' => '1',
                'errorMessage' => $e->getMessage(),
            );
        }
        
        return $data;
    }
}
