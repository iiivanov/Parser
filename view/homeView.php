<!DOCTYPE html>
<html lang="ru">
    <head>
        <title>Youtube Parser</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/main.css" rel="stylesheet">
        <script src="js/main.js"></script>
    </head>
    <body>
        <div class="z-block">
            <h1>Добавить новую строку поиска</h1>
            <form class="addSearchString" id="addSearchString" method="post" action="javascript:void(0);" onsubmit="addSearchString()">
                <div>
                  <label for="searchString">Строка поиска</label>
                  <input type="text" class="searchString" id="searchString" name="searchString" required placeholder="Введите строку поиска" >
                  <button class="addSearchButton">Добавить</button>
                </div>
            </form>
        </div>
        <div class="f-block">
            <h2>Строки поиска</h2>
            <p id="list">
<?php
    if(isset($data['error'])) {
        echo $data['error'];
    } else {
        foreach ($data as $key => $item) {
            echo '<span onclick="search(this)">'.$item['str'].'</span>';
            if($key < count($data)-1) {
                echo ', ';
            }
        }
    }
?>
            </p>
        </div>
        <div class="s-block">
            <ul class="list-avg">
                <li id="autor">-</li>
                <li id="count">-</li>
                <li id="views">-</li>
            </ul>
        </div>
        <div class="t-block">
            <table class="table">
                <thead>
                    <tr>
                        <th>№</th>
                        <th>URL</th>
                        <th>Заголовок</th>
                        <th>Просмотры</th>
                        <th>Описание</th>
                        
                    </tr>
                </thead>
                <tbody id="full-data">
                </tbody>
            </table>
        </div>
    </body>
</html>
