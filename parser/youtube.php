<?php
/**
  * Youtube Parser
  * @category   Parser
  * @author     Ivan Ivanov
  * @author     Ivan Ivanov <iiivanov.ukraine@gmail.com>
  */

class YoutubeParser {
    
    protected $_urlYoutubeSearch;
    protected $_urlVideo = 'https://www.youtube.com/watch?v=';
    protected $_searchCount = 10;
    
    /**
     * 
     */
    function __construct($url = NULL) {
        if(is_null($url)) {
            $this->_urlYoutubeSearch = 'https://www.youtube.com/results?search_query=';
        } else {
            $this->_urlYoutubeSearch = $url;
        }
    }
    
    /**
     * 
     */
    public function setUrlYoutubeSearch($url) {
        $this->_urlYoutubeSearch = $url;
    }
    
    /**
     * 
     */
    public function getUrlYoutubeSearch() {
        return $this->_urlYoutubeSearch;
    }
    
    /**
     * 
     */
    public function setSearchCount($count) {
        $this->_searchCount = $count;
    }
    
    /**
     * 
     */
    public function getSearchCount() {
        return $this->_searchCount;
    }
    
    /**
     * check value
     * 
     * @param string $searchString
     * 
     * @return array
     */
    public function searchVideo($searchString) {
        $i = 0;
        $videos = array();
        
        $doc = new DOMDocument();
        $doc->loadHTML(file_get_contents($this->_urlYoutubeSearch.urlencode($searchString)));      
        
        $elements = $doc->getElementsByTagName('div');
        
        foreach ($elements as $element) {
            if($element->hasAttribute('data-context-item-id')) {
                $videoUrl = $this->_urlVideo.$element->getAttribute('data-context-item-id');

                $headerDOMItem = $this->getTagByAttribute($element, 'a', 'dir', 'ltr');
                $header = $this->getTagValue($headerDOMItem);
                        
                $viewsDOMItem = $this->getTagByAttribute($element, 'ul', 'class', 'yt-lockup-meta-info');
                $views = preg_replace("/(\D)/", "", $this->getTagValue($viewsDOMItem->lastChild));

                $descDOMItem = $this->getTagByAttribute($element, 'div', 'dir', 'ltr');
                $desc = $this->getTagValue($descDOMItem);

                $videos[] = array(
                    'videoUrl' => $videoUrl,
                    'header' => $header,
                    'views' => $views,
                    'desc' => $desc,
                );
                
                $i++;
            }
            if($i == $this->_searchCount) break;
        }
        
        return $videos;
    }
    
    /**
     * @param DOMNodeList $nodeItems
     * @param string $tag
     * @param string $attribute
     * @param string $attributeValue
     * 
     * @return mixed DOMElement or NULL
     */
    public function getTagByAttribute($nodeItems, $tag, $attribute, $attributeValue) {
        
        $items = $nodeItems->getElementsByTagName($tag);
        foreach ($items as $item) {
            if($item->hasAttribute($attribute) AND $item->getAttribute($attribute) == $attributeValue) {
                return $item;
            }
        }
        
        return null;
    }
    
    /**
     * @param DOMNodeList $nodeItem
     * 
     * @return string
     */
    public function getTagValue($nodeItem) {
        return iconv("UTF-8","ISO-8859-1", $nodeItem->nodeValue);
    }
}